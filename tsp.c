#include<stdio.h>
#define MAX 10000
int matrix[9][9];
int dist[9][512];
int check[9];
int N;
int distance(int start, int check) ;
int main()
{
    int T,i, flag1=0;
    scanf("%d", &T);
    if( T > 50 )
        flag1 = 1;
    for( i = 0 ; i < T ; i ++ )
    {
        int j, k, l, a, pm, count, min, flag=0;
        scanf("%d", &N);
        if( N < 3 || N > 8 )
            flag = 1;
        N++;
        for( j = 1 ; j < 9 ; j ++ )
        {
            for( k = 1; k < 9 ; k ++ )
            {
                matrix[j][k] = 0; ;
            }
        }
        for( j = 1 ; j < N ; j ++ )
        {
            for( k = 1; k < N ; k ++ )
            {
                scanf("%d", &a);
                matrix[j][k] = a ;
                if( a > 100 || a< 0 )
                    flag = 1;
            }
        }
        for( j = 1 ; j <N ; j ++ )
        {
            for( k = 1 ; k < N ; k ++ )
            {
                if( !matrix[j][k] )
                    matrix[j][k] = MAX;
            } 
        }
        for( j = 1 ; j < N  ; j ++ )  
        {  
            for( k = 1 ; k < 9 ; k ++ )  
                check[k] = 0;  
 
            check[j]=1;  
            count = N-3 ;
            while( count )  
            {  
                count--;  
                min = MAX ;  
                for( l = 1 ; l < N ; l ++ )  
                {  
                    if( !check[l] && matrix[j][l] < min   )  
                    {  
                        min = matrix[j][l];  
                        pm = l;  
                    }  
                }
                if( min == MAX )
                    continue;
                check[pm]=1;
                for( l = 1 ; l < N ; l ++ )  
                    if( matrix[j][l] > matrix[j][pm]+matrix[pm][l] )  
                    {  
                        matrix[j][l] =  matrix[j][pm]+matrix[pm][l]; 
                    }  
            }  
        }
        for( j = 1 ; j < 9 ; j ++)
            for( k = 1 ; k < 512 ; k ++ )
                dist[j][k] = 0;
        ((a=distance(1,1)) >= MAX || flag | flag1 ) ?  printf("%d", -1) : printf("%d", a);
    }
}
int  distance(int start, int check) 
{  
    int i;
    int sum;  
 
    if(dist[start][check]) return dist[start][check];//최단거리가 이미 기록됨 pass  
    if(check == (1<<N-1)-1) return matrix[start][1];//마지막에 1로 돌아옴  
    dist[start][check] = MAX;  
    for(i = 1; i < N; i++)  
        if( (start != i ) && (!((1<<i-1)&check)) ) 
            // 1 2 3      n = 4, 001  010   100 111 = 2^3 -1 ,2^8-1
            // check 1; 01234  
            // 0은 고정 스타트 check  1  
            // 11 111 1111 1111 11111 
            // path checking  0 1 2 3 4 ,n = 5 ;  
            //1의 check =  0의 check와 겹치지 않는 경로 + 0의 check   
            //check 00001  , 00001 & 00001 = 1 ,  00001 & 00010 = 0// OK  
            //start = 1이 되고, 00011 ,  00001 x 00010 x 00100 o  , start = 2, check = 00111   
            // 11111= 2^5 - 1  
            // path checking 0 4 2 3 1   
            // 00001+ 10000 + 00100 + 01000 + 00010 // OK  
            // N  = 2, check_max = 11 == 2^2-1 == 3;  
            //if N 13 , check_max = 111111111111 == 2^13 - 1 == 8191  
        {  
            sum = matrix[start][i] + distance(i, check + (1<<i-1)) ; //00010 00001  
            if( dist[start][check] > sum ) dist[start][check]=sum;  
        }  
        return dist[start][check];  
}